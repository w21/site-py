#!/usr/bin/python3

import os
import re
import sys

default_filename = os.environ.get('HOME') + "/etc/secrets"

def getsecret(key, fname=default_filename):
    for line in open(fname):
        match = re.match(re.escape(key) + ":(.*)", line.strip())
        if match:
            return match.group(1)
    raise KeyError('cannot find key "{key}" in {fname}'.
                   format(key=key, fname=fname))

if __name__ == '__main__':
    if not (2 <= len(sys.argv) <= 3):
        sys.exit("usage: getsecret key [filename]")
    try:
        print(getsecret(*sys.argv[1:]))
    except Exception as e:
        sys.exit("getsecret: " + str(e))
