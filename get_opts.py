#!/usr/bin/env python3

"""helper for parsing and accessing single-char command-line options

This module offers functions to parse and process the conventional
Unix command-line options -- single-character options given on the
command line directly after the command itself with a leading dash
("-"). These options can be clustered.

They can take arguments, of which this module can handle arbitrary
string arguments as well as integer arguments.

A "--" on the command line that is not an argument to an option
terminates option processing, as is conventional, such that all
remaining arguments are not considered options, but regular
arguments to the program in question. Otherwise, any argument not
starting with "-" and all following it are considered regular,
non-option arguments.

Errors in processing the options will raise a number of exceptions
indicating the type of error. In most cases, only the base class
(OptionError) must be known to the caller.

"""

import re
import sys

class OptionError(Exception):
    """Base class for errors originating from this module.

    Attributes:
        char -- the option character involved
    """
    message = "dummy"

    def __init__(self, char):
        """Create an OptionError exception.

        Parameter: char -- the option char involved in the error
        """
        self.char = char
    def __str__(self):
        return self.message.format(c=self.char)

class InvalidOptionError(OptionError):
    """Raised when an option is seen that is not in the option descriptor."""
    message = "invalid option char `{c}'"

class MissingArgOptionError(OptionError):
    """Raised when an option demands an argument, but has none."""
    message = "missing argument for option `{c}'"

class ArgTypeOptionError(OptionError):
    """Raised when an option argument is not an integer as it should be."""
    message = "argument for option `{c}' is not an integer"

class MultipleOptionError(OptionError):
    """Raised when an option must not be given more than once, but is."""
    message = "option `{c}' seen more than once"

# odesc: { char: [ init_value, arg_type, multiple_ok ], ... }
# arg types: 0: none, but count occurences; 1: want string arg; 2: want int arg
# returns: rest-argv (then call get_values(optdesc) to get the values)
def parse(odesc, argv):
    """Parse single-char command line options.

    This function parses command line options in the traditional
    Unix style, i.e. single-char options that can be clustered and
    may have arguments.

    An argument "--", unless it is the argument to an option, ends
    option parsing, such that all following arguments are not
    processed as option and are returned in the remaining argv. If
    there is not such argument, any argument that is not an option,
    i.e. does not begin with a "-", terminates the options
    processing and make this and all remaining argument regular,
    non-option arguments.
    
    odesc: an option descriptor, which is a dictionary with chars as
           keys and lists of length three as values. They key is the
           option char; the list contains the initial value of the
           option, the argument type coded as a number (0: none, 1:
           string, 2: number), and a flag that is true iff multiple
           occurrences of the option are okay.

           Example: {'v':[1, 0, 1],     # "verbose", may occur multiply
                     'f':["", 1, 0],    # "filename" with string arg
                     'n':[0, 2, 0]}     # "number" with integer arg

    argv: a list containing the command line arguments to parse.
    In most cases, this is sys.argv[1:].

    Return: array with remaining elements of argv not parsed as options

    Also, odesc is updated to reflect the options seen in the first
    elements of the values lists. If the option had an argument, it
    is placed there; if not, the (integer) value is incremented by
    one for each instance of the option seen.

    With the above optstring example and an argv of ["-vvn", "113",
    "-f", "/dev/null", "-v", "he", "hu", "hi"], odesc will be updated
    to      {'v':[4, 0, 1],
             'f':["/dev/null", 1, 0],
             'n':[113, 2, 0]}
    and the return values will be ("", ["he", "hu", "hi"]).

    If the initial value of an option with arguments is a list *and*
    the option may occur multiple times, argument values are
    appended to the list.

    Exceptions:
        OptionError           -- base class for the exceptions of this module
        InvalidOptionError    -- invalid option char seen
        MissingOptionArgError -- an option needs an argument, but none 
                                 is supplied
        OptionArgTypeError    -- an option needs an integer argument,
                                 but the argument is not an integer
        MultipleOptionError   -- an option is seen more than once although
                                 it may occur only once

    On an error, one of these exceptions is raised. Usually, it will
    be enough to exit with the exception error message and a usage
    message:

    try:
        argv = get_opts.parse(odesc, sys.argv)
        ...
    except OptionError as e:
        sys.exit(e + "\n" + usage)

    The exception will be a KeyError for an invalid option char and
    a TypeError if there is no argument for an option demanding an
    argument or a non-integer argument for an option demanding an
    integer argument. This may change in the semi-near future.

    You can use the function get_opts.values(odesc) to extract all
    values from the odesc at once, sorted by option char in ASCII
    order, like this:
        filename, number, verbosity = get_opts.values(odesc)

    """
    seen = set()                # set of options already seen
    while argv and re.match("^-", argv[0]):
        arg = argv.pop(0)
        if arg == "--":
            break
        for c in arg[1:]:
            if c in odesc:
                desc = odesc[c]
            else:
                raise InvalidOptionError(c)
            if c in seen and (len(desc) < 3 or not desc[2]):
                raise MultipleOptionError(c)
            seen.add(c)
            if desc[1] > 0:     # needs argument
                if not argv:
                    raise MissingArgOptionError(c)
                optarg = argv.pop(0)
                if desc[1] == 2:  # needs int argument
                    if not re.match(r"^[+-]?\d+$", optarg):
                        raise ArgTypeOptionError(c)
                    optarg = int(optarg)
                if type(desc[0]) is list:
                    desc[0].append(optarg)
                else:
                    desc[0] = optarg
            else:
                desc[0] += 1
    return argv

# return a sequence of all values in optdesc, in ASCII order of
# the option chars
def values(odesc):
    """Extract the option values from an option descriptor.

    See the documentation for get_opts.parse() for more information.
    """
    return [odesc[c][0] for c in sorted(odesc.keys())]

# return a dictionary of the (option, value) pairs in the option descriptor
# after the options have been parsed
def valuedict(odesc):
    """Return a (option, value) dictionary after parsing options."""
    return {(c, odesc[c][0]) for c in odesc.keys()}

# be backwards compatible with the old names
get_opts = parse
get_values = values

if __name__ == '__main__':
    try:
        case = 0
        odesc1 = {'v':[1, 0, 1],
                  'f':["", 1, 0],
                  'n':[0, 2, 0]}
        case += 1
        args = ["-vvn", "113", "-f", "/dev/null", "-v", "he", "hu", "hi"]
        argv = parse(odesc1, args)
        if not list(values(odesc1)) == ["/dev/null", 113, 4]:
            sys.exit("unexpected optvals: " + str(list(values(odesc1))))
        if not argv == ["he", "hu", "hi"]:
            sys.exit("unexpected argv: " + str(argv))

        case += 1
        args = ["-vvn", "-113", "-f", "/dev/null", "--", "-v", "he", "hu", "hi"]
        argv = parse(odesc1, args)
        if not list(values(odesc1)) == ["/dev/null", -113, 6]:
            sys.exit("unexpected value: " + str(list(values(odesc1))))
        if not argv == ["-v", "he", "hu", "hi"]:
            sys.exit("unexpected argv: " + str(argv))

        case += 1
        args = ["-vvn", "+113", "-f", "/dev/null", "-n", "12",
                "--", "-v", "he", "hu", "hi"]
        odesc1['n'] = [[], 2, 1]
        argv = parse(odesc1, args)
        if not list(values(odesc1)) == ["/dev/null", [113, 12], 8]:
            sys.exit("unexpected value: " + str(list(values(odesc1))))
        if not argv == ["-v", "he", "hu", "hi"]:
            sys.exit("unexpected argv: " + str(argv))
    except Exception as e:
        sys.exit("test case {0}: {1}".format(case, e))

    try:
        program = sys.argv[0].split('/')[-1]
        usage = "usage: {0} [-hvvvq] [-f name] [-n count] arg1 ...".\
                format(program)
        opt_desc = {
            'f': ["", 1, 0],    #filename [empty, string_t, no mult]
            'h': [0, 0, 0],     #help [value 0, count_t, no mult]
            'n': [113, 2, 0],   #count [pre-value, int_t, no mult]
            'q': [0, 0, 0],     #quiet
            'v': [1, 0, 1],     #verbosity
        }

        argv = get_opts(opt_desc, sys.argv[1:])
        opt_file, opt_help, opt_count, opt_quiet, opt_verbose = \
                get_values(opt_desc)
        if opt_help:
            print(usage)
            sys.exit()
        print("opt_file = ", opt_file)
        print("opt_count = ", opt_count)
        print("opt_quiet = ", opt_quiet)
        print("opt_verbose = ", opt_verbose)
        print("argv = ", argv)
        print(repr(valuedict(opt_desc)))
    except Exception as e:
        sys.exit(str(e) + "\n" + usage)
