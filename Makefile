PYTHONLIB = /opt/w21/lib/python3

all:
	@echo
	@echo This Makefile does not have a meaningful default target.
	@echo

test:
	for i in *.py; do ./$$i; done

install:
	rsync -va *.py $(PYTHONLIB)/
	install -c getsecret.py /opt/w21/bin/getsecret
